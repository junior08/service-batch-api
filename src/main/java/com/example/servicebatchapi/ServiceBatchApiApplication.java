package com.example.servicebatchapi;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableBatchProcessing
@ComponentScan({"com.example.servicebatchapi.config",
		"com.example.servicebatchapi.listener",
		"com.example.servicebatchapi.processor",
		"com.example.servicebatchapi.writer",
		"com.example.servicebatchapi.reader"})
public class ServiceBatchApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceBatchApiApplication.class, args);
	}

}
