package com.example.servicebatchapi.config;


import com.example.servicebatchapi.entity.Person;
import com.example.servicebatchapi.listener.JobListener;
import com.example.servicebatchapi.listener.StepListener;
import com.example.servicebatchapi.processor.BatchItemProcessor;
import com.example.servicebatchapi.reader.BatchItemReader;
import com.example.servicebatchapi.writer.BatchItemWritter;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.file.transform.LineTokenizer;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;

import java.io.File;

@Configuration

public class SampleJob {
    private static final Logger log = LoggerFactory.getLogger(SampleJob.class);

    private final BatchItemProcessor batchItemProcessor;
    private final BatchItemReader batchItemReader;
    private final BatchItemWritter batchItemWritter;

    private final JobBuilderFactory jobBuilderFactory;

    private final StepBuilderFactory stepBuilderFactory;

    private final  JobListener jobListener;

    private final StepListener stepListener;

    public SampleJob(BatchItemProcessor batchItemProcessor, BatchItemReader batchItemReader, BatchItemWritter batchItemWritter, JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory, JobListener jobListener, StepListener stepListener) {
        this.batchItemProcessor = batchItemProcessor;
        this.batchItemReader = batchItemReader;
        this.batchItemWritter = batchItemWritter;
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.jobListener = jobListener;
        this.stepListener = stepListener;
    }


    @Bean
    public Job firstJob(){
       return jobBuilderFactory.get("Step Person V-0.1")
               .start(saveBatchStep())
               .incrementer(new RunIdIncrementer()) // if we wont to pass a params
               .listener(jobListener)
               .build();
    }
    public  Step saveBatchStep(){
        return  stepBuilderFactory.get("REGISTER PERSON V-0.1")
                .<Person, Person>chunk(200)
                .reader(personFlatFileItemReader(null))
                .processor(batchItemProcessor)
                .writer(batchItemWritter)
                .build()
                ;
    }

    @Bean
    public FlatFileItemReader<Person> personFlatFileItemReader(@Value("#JobParameters['inputFile']") FileSystemResource filePath) {
        FlatFileItemReader<Person> flatFileItemReader =
                new FlatFileItemReader<>();

        DefaultLineMapper<Person> lineMapper =
                new DefaultLineMapper<>();
        DelimitedLineTokenizer lineTokenizer =
                new DelimitedLineTokenizer();
        BeanWrapperFieldSetMapper<Person> fieldSetMapper =
                new BeanWrapperFieldSetMapper<>();
       // String filePath = "/home/dell/Desktop/batchProcessing/service-batch-api/src/main/resources/static/data-person.csv";
        flatFileItemReader.setResource(filePath);
        lineTokenizer.setNames("NAME", "PHONE", "EMAIL", "ADDRESS", "NUMBERRANGE", "BIRTHDAY");
        lineMapper.setLineTokenizer(lineTokenizer);
        fieldSetMapper.setTargetType(Person.class);
        lineMapper.setFieldSetMapper(fieldSetMapper);
        flatFileItemReader.setLineMapper(lineMapper);
        flatFileItemReader.setLinesToSkip(1);
        return  flatFileItemReader;
    }

    /*
    public Step firstStep(){
      return   stepBuilderFactory.get("My first Step Person")
                .tasklet(firstFasklet())
                .listener(stepListener)
                .build()
        ;
    }

     */



    /*
    private Tasklet firstFasklet(){
        return  new Tasklet() {
            @Override
            public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
                JobParameters jobParameters = new JobParametersBuilder()
                        .addString("khass","mmd-001")
                                .addLong("param2", System.currentTimeMillis())
                                        .toJobParameters();
                chunkContext.getStepContext()
                                .getStepExecution()
                                        .getJobExecution()
                                                .getExecutionContext()
                                                        .put("jobParameters", jobParameters);
                log.info("THIS IS MY FIRST TASKE");
                // ici que je doit faire mes task
                return RepeatStatus.FINISHED;
            }
        };
    }

     */


}


