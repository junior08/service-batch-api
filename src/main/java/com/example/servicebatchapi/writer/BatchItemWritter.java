package com.example.servicebatchapi.writer;

import com.example.servicebatchapi.entity.Person;
import com.example.servicebatchapi.repository.PersonRepository;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class BatchItemWritter implements ItemWriter<Person> {

    private final PersonRepository personRepository;

    public BatchItemWritter(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public void write(List<? extends Person> list) throws Exception {
        list.stream().forEach(personRepository::save);
    }
}
