package com.example.servicebatchapi.processor;

import com.example.servicebatchapi.entity.Person;
import com.example.servicebatchapi.repository.PersonRepository;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;


@Component
public class BatchItemProcessor implements ItemProcessor<Person, Person> {

    private final PersonRepository personRepository;

    public BatchItemProcessor(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public Person process(Person person) throws Exception {
        if(personRepository.findByEmail(person.getEmail()).isPresent()){
            return null;
        }
        person.getName().toUpperCase();
        person.getAddress().toUpperCase();
        person.getNumberrange().toUpperCase();
        return person;
    }
}
