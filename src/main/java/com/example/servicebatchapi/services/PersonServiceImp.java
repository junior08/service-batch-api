package com.example.servicebatchapi.services;

import com.example.servicebatchapi.entity.Person;
import com.example.servicebatchapi.repository.PersonRepository;

public class PersonServiceImp implements  PersonService{

    private final PersonRepository personRepository;

    public PersonServiceImp(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public Person save(Person person) {
        return personRepository.save(person);
    }
}
