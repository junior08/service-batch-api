package com.example.servicebatchapi.services;

import com.example.servicebatchapi.entity.Person;

public interface PersonService {

    Person save(Person person);
}
