package com.example.servicebatchapi.services;

import java.util.List;

public interface JobExecutionExploreService {
    List<String> JobNames();
}
