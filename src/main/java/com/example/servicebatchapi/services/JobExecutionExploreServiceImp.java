package com.example.servicebatchapi.services;

import org.springframework.batch.core.explore.JobExplorer;

import java.util.List;

public class JobExecutionExploreServiceImp implements JobExecutionExploreService{

    private final JobExplorer jobExplorer;

    public JobExecutionExploreServiceImp(JobExplorer jobExplorer) {
        this.jobExplorer = jobExplorer;
    }

    @Override
    public List<String> JobNames() {
        return jobExplorer.getJobNames();
    }

}
